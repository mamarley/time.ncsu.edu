time.ncsu.edu
=============
This is my updated version of the time.ncsu.edu site.  It
has been made to use AJAX to update the time (which also
solves the clock being blank when the page loads) and to
use CSS-based design instead of tables.
