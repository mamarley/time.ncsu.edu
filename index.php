<?php
if($_GET["sync"]!=null){
	print(microtime(true)*1000);
}else{
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>NC State University Official Time</title>
		<style type="text/css">
			body{
				background-color:#ffffff;
				font-family:Helvetica,sans-serif;
			}
			div.outercontainer{
				background-color:#cc0000;
				height:112px;
				width:399px;
				margin-left:auto;
				margin-right:auto;
				margin-bottom:16px;
				padding-top:1px;
			}
			h1.clocktitle{
				font-size:150%;
				font-weight:bolder;
				color:#ffffff;
				text-align:center;
				margin-top:12px;
				margin-bottom:0px;
			}
			div.innercontainer{
				background-color:#ffffff;
				height:30px;
				width:373px;
				margin-left:auto;
				margin-right:auto;
				margin-top:4px;
				padding-top:5px;
			}
			div.timestamp{
				font-size:120%;
				font-weight:normal;
				text-align:center;
			}
			div.disclaimer{
				font-size:75%;
				font-weight:normal;
				color:#000000;
				text-align:center;
			}
		</style>
		<script type="text/javascript">
			<!--
			var curtime=<?= microtime(true)*1000 ?>;
			var daysofweek=['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
			var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
			var xmlhttp=null;
			if(window.XMLHttpRequest){
				xmlhttp=new XMLHttpRequest();
			}else if(window.ActiveXObject){
				xmlhttp=new ActiveXObject("MSXML2.XMLHTTP.3.0");
			}else{
				alert("Browser does not support XMLHttpRequest!");
			}
			function start(){
				drawTime();
				setInterval(syncTime,60000);
				setInterval(updateTime,1000);
			}
			function syncTime(){
				xmlhttp.onreadystatechange=function(){
					if(xmlhttp.readyState==4){
						curtime=parseFloat(xmlhttp.responseText);
					}
				};
				xmlhttp.open("GET","time.php?sync=true",true);
				xmlhttp.send(null);
			}
			function updateTime(){
				curtime+=1000;
				drawTime();
			}
			function drawTime(){
				var curdate=new Date(curtime);
				var ampm="am";
				var hour=curdate.getHours();
				if(hour>12){
					ampm="pm";
					hour-=12;
				}else if(hour==0){
					hour=12;
				}else if(hour==12){
					ampm="pm";
				}
				if(hour<10){
					hour="0"+hour.toString();
				}
				var minutes=curdate.getMinutes();
				if(minutes<10){
					minutes="0"+minutes.toString();
				}
				var seconds=curdate.getSeconds();
				if(seconds<10){
					seconds="0"+seconds.toString();
				}
				var date=curdate.getDate();
				if(date<10){
					date="0"+date.toString();
				}
				document.getElementById("time").innerHTML=daysofweek[curdate.getDay()]+"&nbsp;"+
														  months[curdate.getMonth()]+"&nbsp;"+date+"&nbsp;"+
														  curdate.getFullYear()+"&nbsp;&nbsp;&nbsp;&nbsp;"+
														  hour+":"+ minutes+":"+seconds+"&nbsp;"+ampm;
			}
			-->
		</script>
	</head>
	<body onload="start();">
		<div class="outercontainer">
			<h1 class="clocktitle">NC State University Official Time</h1>
			<div class="innercontainer">
				<div class="timestamp" id="time"></div>
			</div>
		</div>
		<div class="disclaimer">
			<p>
				This clock should keep up with the current official time unless your computer is too busy to handle the time ticks.<br>
				This page will also synchronize every minute to address any clock drift issues.<br><br>
				The current time across the world can be found at <a href='http://www.timeanddate.com/worldclock/'>The World Clock</a>.
			</p>
		</div>
	</body>
</html>
<?php
}
?>
